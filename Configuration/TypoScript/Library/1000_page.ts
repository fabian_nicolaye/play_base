page = PAGE
page {
    10 = FLUIDTEMPLATE
    10 {
        templateRootPaths {
            0 = EXT:bootstrap_package/Resources/Private/Templates/Page/
            1 = {$page.fluidtemplate.templateRootPath}
        }
        partialRootPaths {
            0 = EXT:bootstrap_package/Resources/Private/Partials/Page/
            1 = {$page.fluidtemplate.partialRootPath}
        }
        layoutRootPaths {
            0 = EXT:bootstrap_package/Resources/Private/Layouts/Page/
            1 = {$page.fluidtemplate.layoutRootPath}
        }
        layoutRootPaths.10 = EXT:play_base/Resources/Private/Layouts/
        partialRootPaths.10 = EXT:play_base/Resources/Private/Partials/
        file.stdWrap.cObject = CASE
        file.stdWrap.cObject {
            key {
                data = levelfield:-1, backend_layout, slide
                override.field = backend_layout
            }

            # Standard Layout
            default = TEXT
            default {
                value = EXT:play_base/Resources/Private/Templates/EmptySite.html
            }
        }
    }


    # CSS
    includeCSS {
        blackDashboard = EXT:play_base/Resources/Public/Css/black-dashboard.css
        nucleoIcons = EXT:play_base/Resources/Public/Css/nucleo-icons.css

        css = https://use.fontawesome.com/releases/v5.8.1/css/all.css
             css {
               external = 1
               media = all
               excludeFromConcatenation = 1
              }

        cblBase = {$play_base.page.cssFile}
    }

    includeJSFooterlibs {
        blackDashboard = EXT:play_base/Resources/Public/JavaScript/black-dashboard.js
    }

    includeJS {

        matomo = EXT:play_base/Resources/Public/JavaScript/matomo.js
        matomo {

            type = application/x-javascript
            forceOnTop = 1
            disableCompression = 1
            excludeFromConcatenation = 1
        }
    }
}
[frontend.user.isLoggedIn == 1]
page = PAGE
page {

includeJS {
            zohoChat = EXT:play_base/Resources/Public/JavaScript/zohoChat.js
                zohoChat {
                    type = application/x-javascript
                    forceOnTop = 1
                    disableCompression = 1
                    excludeFromConcatenation = 1
                }
    }
    }
[end]
