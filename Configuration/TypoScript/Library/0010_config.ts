config {
    absRefPrefix = auto
    typolinkEnableLinksAcrossDomains = 1
    spamProtectEmailAddresses = 2

    language = de
    locale_all = de_DE.utf8
    sys_language_uid = 0

    contentObjectExceptionHandler = 0

    compressCss = 1
    compressJs = 1

    concatenateCss = 1
    concatenateJs = 1
    disablePrefixComment = 1
    headerComment >
    cache_clearAtMidnight = 0

    currentURL = TEXT
    currentURL.typolink {
        parameter.data = TSFE:id
        returnLast = url
    }

    page.parent.id = TEXT
    page.parent.id {
        data = page:pid
    }
}

// lib.contentElement {
//     # Overwrite certain templates from fluid_styled_content
//     templateRootPaths.110 = EXT:cbl_base/Resources/Extensions/fluid_styled_content/Templates/
//     partialRootPaths.110 = EXT:cbl_base/Resources/Extensions/fluid_styled_content/Partials/
//     layoutRootPaths.110 = EXT:cbl_base/Resources/Extensions/fluid_styled_content/Layouts/
// }

[globalVar = TSFE:beUserLogin > 0]
config {
    compressCss = 0
    compressJs = 0

    concatenateCss = 0
    concatenateJs = 0
    disablePrefixComment = 0
}
[global]